
class Bank {
    constructor(){
        this.customers = {};
    }

    addCustomer(customerName){
        this.customers[customerName] = 0;
    }

    printAccount(customerName){
        if(customerName in this.customers){
            console.log(`${customerName}: Your account is $${this.customers[customerName]}.`);
        }
        else {
            console.log('Sorry, a customer by that name doesn\'t exist. Please check customer name.')
        }
    }

    deposit(customerName, amount){
        if (customerName in this.customers) {
            this.customers[customerName] += amount;
            console.log(`Thank you ${customerName}. Your new balance is $${this.customers[customerName]}.`);
        }
        else {
            console.log('Sorry, a customer by that name doesn\'t exist. Please check customer name.')
        }
    }

    withdraw(customerName, amount){
        if (customerName in this.customers && this.customers[customerName] >= amount) {
            this.customers[customerName] -= amount;
            console.log(`Enjoy your money ${customerName}. Your new balance is $${this.customers[customerName]}.`);
        }
        else if (this.customers[customerName] < amount){
            console.log(`Sorry ${customerName}, there is not enough money on your account. Your current balance is $${this.customers[customerName]}.`);
        }
        else {
            console.log('Sorry, a customer by that name doesn\'t exist. Please check customer name.')
        }
    }

    printAllCustomers(){
        let listOfAllCustomers = Object.entries(this.customers)
        // console.log(listOfAllCustomers);
        console.log('===== List of all accounts =====')
        listOfAllCustomers.forEach((array)=>{
            console.log(`${array[0]}, current balance is $${array[1]}.`);
        });
    }
}


let bank = new Bank()


bank.addCustomer('Selim');
bank.addCustomer('Pablo');
bank.printAccount('Pablo');
bank.printAccount('Jerkass');
bank.addCustomer('Sheldon');
bank.printAccount('Sheldon');
bank.deposit('Sheldon', 10);
bank.printAccount('Sheldon');
bank.addCustomer('Raj');
bank.printAccount('Raj');
bank.deposit('Raj', 10000);
bank.printAccount('Raj');
bank.withdraw('Raj', 100);
bank.printAccount('Sheldon');
bank.printAccount('Raj'); 
bank.withdraw('Sheldon', 50)

bank.printAllCustomers();